In order to run the tests you need
1. Java Development Kit - I used 15.0.1
2. Maven
3. Selenium Web Driver
4. ChromeDriver - I used 87.0.4280.88 - my standard path to chromedriver is src/main/resources/chromedriver.exe
    If your path is different you need to change it in test code -
     System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
5. JUnit
6. As IDE I strongly recommend IntelliJ Idea (with Maven included)

How to run the tests:
1. Pull the project with GIT - 
2. Test code is located in Java_Selenium/src/test/java/com.netguru.automationpractice/
3. Open the tests files with your IDE and wait a little bit until Maven configures all dependencies (reloading Maven might be necessary) 
4. Click on run to run the tests.


